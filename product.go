package suresupplytypes

import (
	"context"
	"fmt"
	"log"
	"os"
	"reflect"
	"time"

	"cloud.google.com/go/firestore"
)

const (
	MATCHPATH   = "symbols/product/amzmatch/%s"
	PRODUCTPATH = "symbols/product/symbol/%s"
)

type Product struct {
	// Match
	ASIN            string    `match:"asin" `
	UPC             string    `match:"upc"`
	SalesRank       int64     `match:"salesrank"`
	Category        string    `match:"category"`
	Brand           string    `match:"brand"`
	Manufacturer    string    `match:"manufacturer"`
	PkgQuantity     int64     `match:"pkgquantity"`
	SmallImage      string    `match:"smallImage"`
	Title           string    `match:"title"`
	ListPrice       int64     `match:"listPrice"`
	LastMatchLookup time.Time `match:"lastMatchLookup"`
	FBAFees         int64     `match:"fbafees"`

	//Offer
	LastOfferLookup time.Time `offer:"lastOfferLookup"`
	BBW             bool      `offer:"bbw"`
	SFBR            float64   `offer:"sfbr"`
	SFBC            int64     `offer:"sfbc"`
	InStock         string    `offer:"inStock"`
	CPIP            int64     `offer:"cpip"`
	FBA             bool      `offer:"fba"`
}

func (p *Product) Publish(subtype string) error {
	if p.UPC == "" {
		return fmt.Errorf("%s", "The upc is empty")
	}
	fsClient, err := firestore.NewClient(context.Background(), os.Getenv("GCP_PROJECT"))
	if err != nil {
		log.Printf(err.Error())
		return err
	}

	// If its a match type write to the match key to trigger the other updates
	batch := fsClient.Batch()
	if subtype == "match" {
		batch.Set(fsClient.Doc(fmt.Sprintf(MATCHPATH, p.UPC)), p.getMSOf(subtype), firestore.MergeAll)
	}

	// Write to the product path no one is listening to this.

	batch.Set(fsClient.Doc(fmt.Sprintf(PRODUCTPATH, p.UPC)), p.getMSOf(subtype), firestore.MergeAll)
	_, err = batch.Commit(context.Background())
	return err
}

// getMSOf get s map string of the struct so that it can be leveraged in
// the firestore mergeall command and be targeted by tag
func (p *Product) getMSOf(tagName string) map[string]interface{} {
	result := make(map[string]interface{})

	// Reflect the type
	t := reflect.TypeOf(*p)
	s := reflect.ValueOf(p).Elem()
	// Loop the fields looking for the tag
	for i := 0; i < t.NumField(); i++ {
		fv := s.Field(i)
		ft := t.Field(i)
		if propName, ok := ft.Tag.Lookup(tagName); ok {
			result[propName] = getValueInterface(fv)
		}
	}
	return result
}

// Reflect the value to return the correct interface
func getValueInterface(val reflect.Value) interface{} {
	// Returning a something now
	switch val.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return val.Int()
	case reflect.Float64:
		return val.Float()
	case reflect.Bool:
		return val.Bool()
	case reflect.String:
		return val.String()
	case reflect.Struct:
		return val.Interface()

	default:
		return fmt.Sprintf("%+v", reflect.TypeOf(val))
	}
}

type Products []Product

func (a Products) Len() int           { return len(a) }
func (a Products) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a Products) Less(i, j int) bool { return a[i].SalesRank < a[j].SalesRank }
