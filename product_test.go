package suresupplytypes

import (
	"os"
	"strconv"
	"testing"
	"time"
)

//////
func TestMSOf(t *testing.T) {
	os.Setenv("GCP_PROJECT", "sure-supply")
	tp := Product{
		// Match
		ASIN:         "abc123",
		UPC:          strconv.FormatInt(time.Now().UnixNano(), 10),
		SalesRank:    1234567890,
		Category:     "def456",
		Brand:        "def456",
		Manufacturer: "def456",
		PkgQuantity:  1234567890,
		SmallImage:   "def456",
		Title:        "def456",
		ListPrice:    1234567890,
		// Offers
		LastOfferLookup: time.Now(),
		BBW:             true,
		SFBR:            99.99,
		SFBC:            1234567890,
		InStock:         "NOW",
		CPIP:            1234567890,
		FBA:             true,
	}
	if err := tp.Publish("match"); err != nil {
		t.Fatalf(err.Error())
	}
	if err := tp.Publish("offer"); err != nil {
		t.Fatalf(err.Error())
	}

}
